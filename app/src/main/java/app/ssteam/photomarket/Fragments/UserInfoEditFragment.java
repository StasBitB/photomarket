package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;

import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.Dialogs.PhotoDialog;
import app.ssteam.photomarket.Dialogs.ProgressDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.HTTP.Models.UserInformation;
import app.ssteam.photomarket.Helper.BitmapHelper;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.Loaders.BitmapLoader;
import app.ssteam.photomarket.R;
import app.ssteam.photomarket.Tasks.CallBacks.FragmentCallback;
import app.ssteam.photomarket.Tasks.CropUserPhotoTask;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 13-Apr-15.
 */
public class UserInfoEditFragment extends AttachedFragment implements LoaderManager.LoaderCallbacks<Bitmap> {

    private static final int SELECT_PHOTO = 100;
    private static final int TAKE_PHOTO = 101;

    private UserInformation mUserInformation;
    private PhotoModel mPhotoModel;
    private Uri mSelectedImage;
    private EditText mNameField;
    private EditText mSecondNameField;
    private EditText mCityField;
    private EditText mStatusField;
    private EditText mEmailField;
    private String mLanguageField;
    private ImageView mPhotoImage;
    private ProgressBar mPhotoProgress;
    private Spinner mLanguageSpinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mUserInformation = UserDataHelper.loadUserObject(getActivity());
        mPhotoModel = mUserInformation.photo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_user_edit_info, container, false);

        mPhotoImage = (ImageView) root.findViewById(R.id.photoImage);
        mPhotoImage.setOnClickListener(selectPhoto);
        mNameField = (EditText) root.findViewById(R.id.nameField);
        mSecondNameField = (EditText) root.findViewById(R.id.secondNameField);
        mCityField = (EditText) root.findViewById(R.id.cityField);
        mStatusField = (EditText) root.findViewById(R.id.statusField);
        mEmailField = (EditText) root.findViewById(R.id.emailField);
        mPhotoProgress = (ProgressBar) root.findViewById(R.id.photoProgress);

        String url = UserDataHelper.loadUserData(getActivity()).photoUrl;
        Picasso.with(getActivity()).load(url).resize(350, 350).centerCrop().into(mPhotoImage);
        initLanguageSpinner(root);
        initUserData();

        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_edit), getActivity());

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
            case TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {

                    mSelectedImage = imageReturnedIntent.getData();

                    try {
                        BitmapHelper.rotateOnPortrait(getActivity(), mSelectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    loadImage();

                    CropUserPhotoTask cropUserPhotoTask = new CropUserPhotoTask(new FragmentCallback() {
                        @Override
                        public void onTaskDone(PhotoModel model) {
                            UserDataHelper.saveUserPhotoUrl(getActivity(), model.url);
                            mPhotoProgress.setVisibility(View.INVISIBLE);
                            mPhotoModel = model;
                        }
                    }, getActivity());

                    mPhotoProgress.setVisibility(View.VISIBLE);
                    cropUserPhotoTask.execute(mSelectedImage);

                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.saveProfile:
                onSave();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_info_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void onSave() {

        if (mPhotoModel == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.toast_image_upload), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!getStringEditText(mNameField).isEmpty() && !getStringEditText(mSecondNameField).isEmpty() && !getStringEditText(mCityField).isEmpty() && !getStringEditText(mStatusField).isEmpty()
                && !getStringEditText(mEmailField).isEmpty() && !mLanguageField.isEmpty()) {

            UserEntity user = UserDataHelper.loadUserData(getActivity());

            final DialogFragment dialog = new ProgressDialog();
            dialog.show(getFragmentManager(), ProgressDialog.PROGRESS_DIALOG_TAG);

            HttpRequestAPI api = HttpClient.getInstance();
            api.setUserInformation(user.token, user.id, mPhotoModel.id, getStringEditText(mNameField), getStringEditText(mSecondNameField), mLanguageField,
                    getStringEditText(mCityField), getStringEditText(mStatusField), new Callback<UserInformation>() {


                        @Override
                        public void success(UserInformation userInformation, Response response) {
                            if (!isAttached) return;
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            dialog.dismiss();
                            DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(error));
                            dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                        }


                    });
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.toast_check_input), Toast.LENGTH_SHORT).show();
        }
    }


    private View.OnClickListener selectPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment dialog = PhotoDialog.newInstance(new PhotoDialog.DialogResult() {

                @Override
                public void result(int source) {
                    switch (source) {
                        case PhotoDialog.CAMERA:
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            startActivityForResult(intent, TAKE_PHOTO);
                            break;
                        case PhotoDialog.GALLERY:
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                            break;
                    }
                }
            });
            dialog.show(getFragmentManager(), PhotoDialog.PHOTO_DIALOG_TAG);
        }
    };


    private String getStringEditText(EditText edittext) {
        return edittext.getText().toString();
    }

    private void initUserData() {
        mNameField.setText(mUserInformation.first_name);
        mSecondNameField.setText(mUserInformation.last_name);
        mCityField.setText(mUserInformation.town);
        mStatusField.setText(mUserInformation.status);
        mEmailField.setText(mUserInformation.user.email);
    }

    private void initLanguageSpinner(View root) {
        mLanguageSpinner = (Spinner) root.findViewById(R.id.languageSpinner);

        final ArrayAdapter<CharSequence> dataAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.lang, R.layout.simple_spinner_item);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        mLanguageSpinner.setAdapter(dataAdapter);
        mLanguageSpinner.setSelection(0);
        mLanguageField = dataAdapter.getItem(0).toString();

        mLanguageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLanguageField = dataAdapter.getItem(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadImage() {
        if (mSelectedImage != null)
            getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<Bitmap> onCreateLoader(int id, Bundle args) {
        return new BitmapLoader(getActivity(), mSelectedImage);
    }

    @Override
    public void onLoadFinished(Loader<Bitmap> loader, Bitmap data) {
        if (isAttached) mPhotoImage.setImageBitmap(data);
    }

    @Override
    public void onLoaderReset(Loader<Bitmap> loader) {

    }

}
