package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import app.ssteam.photomarket.Activities.GenericActivity;
import app.ssteam.photomarket.Activities.UserEditInfoActivity;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.UserAlbumList;
import app.ssteam.photomarket.HTTP.Models.UserInformation;
import app.ssteam.photomarket.HTTP.Models.UserList;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 27-Mar-15.
 */
public class UserInfoFragment extends RequestFragment {

    public final static String FRIEND_ID = "FriendId";

    private TextView mFriendsCount;
    private TextView mFollowersCount;
    private TextView mAlbumsCount;
    private TextView mEmailField;
    private TextView mNameField;
    private TextView mStatusField;
    private TextView mCityField;
    private TextView mBirthFieild;
    private TextView mLanguageField;
    private FloatingActionButton mEditButton;
    private ImageView mPhotoImage;
    private LinearLayout mFriendsButton;
    private LinearLayout mFollowersButton;
    private LinearLayout mAlbumsButton;

    public static UserInfoFragment newInstance(Bundle bundle) {
        UserInfoFragment fragment = new UserInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected View setMainView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rootView = inflater.inflate(R.layout.fragment_user_information, null);

        mPhotoImage = (ImageView) rootView.findViewById(R.id.photoImage);
        mEditButton = (FloatingActionButton) rootView.findViewById(R.id.editButton);

        if (getFriendId() == -1) {
            mEditButton.setTitle(getResources().getString(R.string.edit_button_title));
            mEditButton.setPadding(0, 0, 50, 50);
            mEditButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), UserEditInfoActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            mEditButton.setVisibility(View.GONE);
        }

        mFriendsCount = (TextView) rootView.findViewById(R.id.friendsCount);
        mFollowersCount = (TextView) rootView.findViewById(R.id.followersCount);
        mAlbumsCount = (TextView) rootView.findViewById(R.id.albumsCount);
        mEmailField = (TextView) rootView.findViewById(R.id.emailField);
        mStatusField = (TextView) rootView.findViewById(R.id.statusField);
        mNameField = (TextView) rootView.findViewById(R.id.nameField);
        mCityField = (TextView) rootView.findViewById(R.id.cityField);
        mBirthFieild = (TextView) rootView.findViewById(R.id.birthdayField);
        mLanguageField = (TextView) rootView.findViewById(R.id.languageField);
        mFriendsButton = (LinearLayout) rootView.findViewById(R.id.friendsButton);
        mFollowersButton = (LinearLayout) rootView.findViewById(R.id.followersButton);
        mAlbumsButton = (LinearLayout) rootView.findViewById(R.id.albumsButton);

        mFriendsButton.setOnClickListener(getListener(GenericActivity.FRIEND_LIST_FRAGMENT));
        mFollowersButton.setOnClickListener(getListener(GenericActivity.REQUEST_INCOME_LIST_FRAGMENT));
        mAlbumsButton.setOnClickListener(getListener(GenericActivity.ALBUM_LIST_FRAGMENT));

        return rootView;
    }

    @Override
    protected String setTitle() {
        return getResources().getString(R.string.action_bar_profile);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onLoad() {
        showLoading();
        UserEntity user = UserDataHelper.loadUserData(getActivity());

        HttpRequestAPI api = HttpClient.getInstance();

        api.getUserInformation(user.token, user.id, getFriendId(), new Callback<UserInformation>() {
            @Override
            public void success(UserInformation userInformation, Response response) {
                if (!isAttached) return;
                mNameField.setText(userInformation.first_name + "  " + userInformation.last_name);
                mStatusField.setText(userInformation.status);
                mCityField.setText(userInformation.town);
                mLanguageField.setText(userInformation.language);
                mEmailField.setText(userInformation.user.email);
                String url = HttpClient.get_SERVER_URL() + userInformation.photo.url;
                UserDataHelper.saveUserPhotoUrl(getActivity(), url);
                Picasso.with(getActivity()).load(url).resize(350, 350).centerCrop().into(mPhotoImage);
                showView();
                UserDataHelper.saveUserObject(getActivity(), userInformation);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                showError();
                DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(retrofitError));
                dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
            }
        });
        loadAdditionalInfo();
    }

    private void loadAdditionalInfo() {
        HttpRequestAPI api = HttpClient.getInstance();
        UserEntity user = UserDataHelper.loadUserData(getActivity());

        api.friendsList(user.token, user.id, getFriendId(), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {
                if (!isAttached) return;
                mFriendsCount.setText(Integer.toString(userList.content.size()));
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        api.incomeRequestList(user.token, user.id, getFriendId(), new Callback<UserList>() {
            @Override
            public void success(UserList userList, Response response) {
                if (!isAttached) return;
                mFollowersCount.setText(Integer.toString(userList.content.size()));
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        api.getUserAlbums(user.token, user.id, getFriendId(), new Callback<UserAlbumList>() {
            @Override
            public void success(UserAlbumList userAlbumList, Response response) {
                if (!isAttached) return;
                mAlbumsCount.setText(Integer.toString(userAlbumList.content.size()));
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });

    }

    private View.OnClickListener getListener(final String fragment) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString(GenericActivity.FRAGMENT_KEY, fragment);
                if (getArguments() != null) {
                    bundle.putInt(FRIEND_ID, getFriendId());
                }
                intent.putExtras(bundle);
                intent.setClass(getActivity(), GenericActivity.class);
                startActivity(intent);
            }
        };
    }

    private int getFriendId() {
        if (getArguments() != null)
            return getArguments().getInt(FRIEND_ID, -1);
        else return -1;
    }
}
