package app.ssteam.photomarket.Fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import app.ssteam.photomarket.R;

/**
 * Created by Stas on 07-Apr-15.
 */
public abstract class RequestFragment extends AttachedFragment {

    private LinearLayout mLoadingLayout;
    private LinearLayout mErrorLayout;
    private RelativeLayout mPreviewLayout;

    protected abstract View setMainView();

    protected abstract String setTitle();

    protected abstract void onLoad();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_request, container, false);

        mLoadingLayout = (LinearLayout) rootView.findViewById(R.id.loadingLayout);
        mErrorLayout = (LinearLayout) rootView.findViewById(R.id.errorLayout);
        mPreviewLayout = (RelativeLayout) rootView.findViewById(R.id.previewLayout);

        mPreviewLayout.addView(setMainView());

        mErrorLayout.setOnClickListener(onReload);
        if (setTitle() != null)
            ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(setTitle());

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onLoad();
    }

    private View.OnClickListener onReload = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onLoad();
        }
    };

    protected void showView() {
        mLoadingLayout.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.GONE);
        mPreviewLayout.setVisibility(View.VISIBLE);
    }

    protected void showError() {
        mLoadingLayout.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.VISIBLE);
        mPreviewLayout.setVisibility(View.GONE);
    }

    protected void showLoading() {
        mLoadingLayout.setVisibility(View.VISIBLE);
        mErrorLayout.setVisibility(View.GONE);
        mPreviewLayout.setVisibility(View.GONE);
    }

}
