package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import app.ssteam.photomarket.Loaders.BitmapLoader;
import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Activities.Interfaces.NavigationCallBack;
import app.ssteam.photomarket.Dialogs.PhotoDialog;
import app.ssteam.photomarket.HTTP.Models.PersonModel;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.HTTP.Models.RegisterModel;
import app.ssteam.photomarket.Helper.BitmapHelper;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.R;
import app.ssteam.photomarket.Tasks.CallBacks.FragmentCallback;
import app.ssteam.photomarket.Tasks.CropUserPhotoTask;

/**
 * Created by Stas on 23-Mar-15.
 */
public class SignUpUserInfoFragment extends AttachedFragment implements LoaderManager.LoaderCallbacks<Bitmap> {

    private static final int SELECT_PHOTO = 100;
    private static final int TAKE_PHOTO = 101;

    private RadioGroup mRadioSexGroup;
    private RadioButton mRadioSexButton;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mEmail;
    private ImageView mPhotoImage;
    private TextView mConfirmButton;
    private Uri mSelectedImage;
    private ProgressBar mPhotoProgress;
    private RegisterModel mRegisterModel;
    private NavigationCallBack mNavigationCallBack;
    private CropUserPhotoTask mCropUserPhotoTask;

    public static SignUpUserInfoFragment newInstance(NavigationCallBack mNavigationCallBack) {
        SignUpUserInfoFragment signUpUserInfoFragment = new SignUpUserInfoFragment();
        signUpUserInfoFragment.mNavigationCallBack = mNavigationCallBack;
        return signUpUserInfoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(false);
        mRegisterModel = new RegisterModel();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadImage();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mCropUserPhotoTask != null) mCropUserPhotoTask.cancel(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        mConfirmButton = (TextView) view.findViewById(R.id.confirmButton);
        mPhotoImage = (ImageView) view.findViewById(R.id.photoImage);
        mPhotoImage.setOnClickListener(selectPhoto);
        mFirstName = (EditText) view.findViewById(R.id.FirstNameField);
        mLastName = (EditText) view.findViewById(R.id.LastNameField);
        mEmail = (EditText) view.findViewById(R.id.emailField);
        mConfirmButton.setOnClickListener(onConfirm);
        mRadioSexGroup = (RadioGroup) view.findViewById(R.id.radioSex);
        mPhotoProgress = (ProgressBar) view.findViewById(R.id.photoProgress);
        int selectedId = mRadioSexGroup.getCheckedRadioButtonId();
        mRadioSexButton = (RadioButton) view.findViewById(selectedId);

        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_sign_up), getActivity());

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private View.OnClickListener onConfirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (mRegisterModel.photo == null) {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast_image_upload), Toast.LENGTH_SHORT).show();
                return;
            }

            if (!isEditTextEmpty(mFirstName) && !isEditTextEmpty(mLastName) && !isEditTextEmpty(mEmail)) {
                mRegisterModel.person = new PersonModel();
                mRegisterModel.person.setFirstName(getStringEditText(mFirstName));
                mRegisterModel.person.setLastName(getStringEditText(mLastName));
                mNavigationCallBack.forwardStack(mRegisterModel);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast_check_input), Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
            case TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    mSelectedImage = imageReturnedIntent.getData();

                    try {
                        BitmapHelper.rotateOnPortrait(getActivity(), mSelectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    loadImage();

                    mCropUserPhotoTask = new CropUserPhotoTask(new FragmentCallback() {
                        @Override
                        public void onTaskDone(PhotoModel model) {
                            if (!isAttached) return;
                            mPhotoProgress.setVisibility(View.INVISIBLE);
                            UserDataHelper.saveUserPhotoUrl(getActivity(), model.url);
                            mRegisterModel.photo = model;
                        }
                    }, getActivity());

                    mRegisterModel.photo = null;
                    mPhotoProgress.setVisibility(View.VISIBLE);
                    mCropUserPhotoTask.execute(mSelectedImage);
                }
                break;
        }

    }

    private View.OnClickListener selectPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment dialog = PhotoDialog.newInstance(new PhotoDialog.DialogResult() {

                @Override
                public void result(int source) {
                    switch (source) {
                        case PhotoDialog.CAMERA:
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            startActivityForResult(intent, TAKE_PHOTO);
                            break;
                        case PhotoDialog.GALLERY:
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                            break;
                    }
                }
            });
            dialog.show(getFragmentManager(), PhotoDialog.PHOTO_DIALOG_TAG);

        }
    };

    private boolean isEditTextEmpty(EditText edittext) {
        return edittext.getText().toString().isEmpty();
    }

    private String getStringEditText(EditText edittext) {
        return edittext.getText().toString();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("uri", String.valueOf(mSelectedImage));
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            mSelectedImage = Uri.parse(savedInstanceState.getString("uri"));
    }

    private void loadImage() {
        if (mSelectedImage != null)
            getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<Bitmap> onCreateLoader(int id, Bundle args) {
        return new BitmapLoader(getActivity(), mSelectedImage);
    }

    @Override
    public void onLoadFinished(Loader<Bitmap> loader, Bitmap data) {
        if (isAttached) mPhotoImage.setImageBitmap(data);
    }

    @Override
    public void onLoaderReset(Loader<Bitmap> loader) {

    }
}
