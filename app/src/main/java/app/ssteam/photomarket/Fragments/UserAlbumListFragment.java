package app.ssteam.photomarket.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;

import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import app.ssteam.photomarket.Activities.NewAlbumActivity;
import app.ssteam.photomarket.Adapters.AdapterEntities.AlbumGridViewItem;
import app.ssteam.photomarket.Adapters.AlbumsGridViewAdapter;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.UserAlbum;
import app.ssteam.photomarket.HTTP.Models.UserAlbumList;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 07-Apr-15.
 */
public class UserAlbumListFragment extends RequestFragment {

    private FloatingActionButton mAddButton;
    private GridView mAlbumsGrid;
    private AlbumsGridViewAdapter mAlbumsAdapter;

    public static UserAlbumListFragment newInstance(Bundle bundle) {
        UserAlbumListFragment fragment = new UserAlbumListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected View setMainView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.fragment_user_album_list, null);

        mAlbumsGrid = (GridView) rootView.findViewById(R.id.albumsGrid);
        mAddButton = (FloatingActionButton) rootView.findViewById(R.id.addButton);
        if (getFriendId() == -1) {
            mAddButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), NewAlbumActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            mAddButton.setVisibility(View.GONE);
        }

        mAlbumsAdapter = new AlbumsGridViewAdapter(getActivity(), new ArrayList<AlbumGridViewItem>());
        mAlbumsGrid.setAdapter(mAlbumsAdapter);
        mAlbumsGrid.setOnItemClickListener(mAlbumsAdapter);

        return rootView;
    }

    @Override
    protected String setTitle() {
        return getResources().getString(R.string.action_bar_albums);
    }

    @Override
    protected void onLoad() {
        showLoading();

        UserEntity user = UserDataHelper.loadUserData(getActivity());

        HttpRequestAPI api = HttpClient.getInstance();
        api.getUserAlbums(user.token, user.id, getFriendId(), new Callback<UserAlbumList>() {

            @Override
            public void success(UserAlbumList userAlbumList, Response response) {
                if (!isAttached) return;
                ArrayList<AlbumGridViewItem> data = new ArrayList<>();
                for (int j = 0; j < userAlbumList.content.size(); j++) {
                    UserAlbum userAlbum = userAlbumList.content.get(j);
                    data.add(new AlbumGridViewItem(userAlbum.id, userAlbum.title, userAlbum.description, userAlbum.icon.url));
                }
                mAlbumsAdapter.setData(data);
                showView();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                showError();
                DialogFragment dialog = InfoDialog.newInstance(retrofitError.getMessage());
                dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
            }
        });

    }

    private int getFriendId() {
        if (getArguments() != null)
            return getArguments().getInt(UserInfoFragment.FRIEND_ID, -1);
        else return -1;
    }
}
