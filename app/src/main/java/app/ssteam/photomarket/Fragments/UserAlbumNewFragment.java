package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.util.UUID;

import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Adapters.AdapterEntities.PhotoGridViewItem;
import app.ssteam.photomarket.Adapters.PhotoGridViewAdapter;
import app.ssteam.photomarket.CustomViews.ExpandableGridView;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.HTTP.Models.UserAlbum;
import app.ssteam.photomarket.Helper.BitmapHelper;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import app.ssteam.photomarket.Services.UploadService;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 07-Apr-15.
 */
public class UserAlbumNewFragment extends AttachedFragment {

    private static final int SELECT_PHOTO = 100;

    private UploadService mUploadService;

    private ExpandableGridView mPhotoGrid;
    private TextView mPickUpPhotoButton;
    private EditText mTittleField;
    private EditText mDescriptionField;
    private PhotoGridViewAdapter mAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_new_album, container, false);

        mPickUpPhotoButton = (TextView) root.findViewById(R.id.pickUpPhoto);
        mPickUpPhotoButton.setOnClickListener(onPickUp);
        mTittleField = (EditText) root.findViewById(R.id.tittleField);
        mDescriptionField = (EditText) root.findViewById(R.id.descriptionField);

        mPhotoGrid = (ExpandableGridView) root.findViewById(R.id.myId);
        mPhotoGrid.setExpanded(true);
        mAdapter = new PhotoGridViewAdapter(getActivity());
        mPhotoGrid.setAdapter(mAdapter);

        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_new_album), getActivity());

        Intent intent = new Intent(getActivity(), UploadService.class);
        getActivity().startService(intent);

        return root;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.saveAlbum:

                if (mAdapter.getUploadedIds().size() != mAdapter.getCount()) {
                    showDialog(getResources().getString(R.string.dialog_wait_upload));
                    return true;
                } else if (mAdapter.getCount() == 0) {
                    showDialog(getResources().getString(R.string.dialog_select_image));
                    return true;
                }

                if (!getStringEditText(mTittleField).isEmpty() && !getStringEditText(mDescriptionField).isEmpty()) {

                    UserEntity user = UserDataHelper.loadUserData(getActivity());

                    HttpRequestAPI api = HttpClient.getInstance();
                    api.createNewAlbum(user.token, user.id, getStringEditText(mTittleField),
                            getStringEditText(mDescriptionField), mAdapter.getUploadedIds(), new Callback<UserAlbum>() {
                                @Override
                                public void success(UserAlbum album, Response response) {
                                    mUploadService.cleanList();
                                    if (!isAttached) return;
                                    getActivity().onBackPressed();
                                }

                                @Override
                                public void failure(RetrofitError retrofitError) {
                                    DialogFragment dialog = InfoDialog.newInstance(retrofitError.getMessage());
                                    dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                                }
                            });
                } else {
                    DialogFragment dialog = InfoDialog.newInstance(getResources().getString(R.string.dialog_fill_album_data));
                    dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.album_new_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(getActivity(), UploadService.class);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unbindService(mConnection);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            UploadService.LocalBinder localBinder = (UploadService.LocalBinder) service;
            mUploadService = localBinder.getService();
            mAdapter.addUploaded(mUploadService.getUploadedList());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {

                    Uri selectedImage = imageReturnedIntent.getData();
                    String path = BitmapHelper.getRealPathFromURI(getActivity(), selectedImage);
                    String fileName = path.substring(path.lastIndexOf("/") + 1).split("\\.")[0];
                    Bitmap preview = null;
                    try {
                        preview = BitmapHelper.decodeUri(getActivity(), selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    PhotoGridViewItem photoGridViewItem = new PhotoGridViewItem(preview, fileName);
                    mAdapter.addPhotoToUpload(photoGridViewItem);

                    mUploadService.addImageToQueue(selectedImage, photoGridViewItem.uuid);
                }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(UploadService.BROADCAST_NOTIFICATION);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int id = intent.getIntExtra(UploadService.ID_UPLOADED, -1);
            UUID uuid = (UUID) intent.getSerializableExtra(UploadService.TEMP_UUID);
            PhotoModel model = mUploadService.getUploadedById(id);
            mAdapter.setUploaded(uuid, model);
        }
    };

    private String getStringEditText(EditText edittext) {
        return edittext.getText().toString();
    }

    private View.OnClickListener onPickUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);

        }
    };

    private void showDialog(String message) {
        DialogFragment dialog = InfoDialog.newInstance(message);
        dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
    }

}
