package app.ssteam.photomarket.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.ssteam.photomarket.Activities.LoginActivity;
import app.ssteam.photomarket.Activities.NavigationActivity;
import app.ssteam.photomarket.Activities.SignUpActivity;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 23-Mar-15.
 */
public class LoadFragment extends Fragment {

    private UserEntity mUser;
    private TextView mSignUp;
    private TextView mLogIn;
    private LinearLayout mLoginLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUser = UserDataHelper.loadUserData(getActivity());
        if (mUser.id != -1 && !mUser.token.isEmpty()) {
            Intent intent = new Intent();
            intent.setClass(getActivity(), NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_load, container, false);

        mLoginLayout = (LinearLayout) view.findViewById(R.id.loginLayout);
        mSignUp = (TextView) view.findViewById(R.id.sign_up);
        mLogIn = (TextView) view.findViewById(R.id.log_in);
        mSignUp.setOnClickListener(signUpListener);
        mLogIn.setOnClickListener(logInListener);
        mLoginLayout.setVisibility(View.VISIBLE);


        return view;
    }

    private View.OnClickListener signUpListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(getActivity(), SignUpActivity.class);
            startActivity(intent);
        }
    };

    private View.OnClickListener logInListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(getActivity(), LoginActivity.class);
            startActivity(intent);
        }
    };
}
