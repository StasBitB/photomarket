package app.ssteam.photomarket.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Activities.NavigationActivity;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.LogInInfo;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 23-Mar-15.
 */
public class LoginFragment extends AttachedFragment {

    private TextView mLogInButton;
    private EditText mLoginField;
    private EditText mPasswordField;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_log_in, container, false);

        mLogInButton = (TextView) view.findViewById(R.id.LogInButton);
        mLoginField = (EditText) view.findViewById(R.id.loginField);
        mPasswordField = (EditText) view.findViewById(R.id.passwordField);
        mLogInButton.setOnClickListener(onLogInListener);

        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_log_in), getActivity());
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private View.OnClickListener onLogInListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isEditTextEmpty(mLoginField) && !isEditTextEmpty(mPasswordField)) {

                HttpRequestAPI api = HttpClient.getInstance();
                api.LoginUser(getStringEditText(mLoginField), getStringEditText(mPasswordField), new Callback<LogInInfo>() {
                    @Override
                    public void success(LogInInfo logInInfo, Response response) {
                        if (!isAttached) return;
                        if (logInInfo.status == 1) {

                            UserDataHelper.saveUserIdToken(getActivity(), logInInfo.userId, logInInfo.token);
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), NavigationActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();

                        } else {
                            DialogFragment dialog = InfoDialog.newInstance(logInInfo.error);
                            dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        DialogFragment dialog = InfoDialog.newInstance(retrofitError.getMessage());
                        dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                    }
                });


            }
        }
    };

    private String getStringEditText(EditText edittext) {
        return edittext.getText().toString();
    }

    private boolean isEditTextEmpty(EditText edittext) {
        return edittext.getText().toString().isEmpty();
    }

}
