package app.ssteam.photomarket.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Activities.Interfaces.NavigationCallBack;
import app.ssteam.photomarket.Activities.NavigationActivity;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.LogInInfo;
import app.ssteam.photomarket.HTTP.Models.RegisterModel;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 20-Jun-15.
 */
public class SignUpUserDetailsFragment extends AttachedFragment {

    private Spinner mLanguageSpinner;
    private TextView mConfirmButton;
    private EditText mCityField;
    private EditText mStatusField;
    private String mLanguageField;
    private RegisterModel mRegisterModel;
    private NavigationCallBack mNavigationCallBack;

    public static SignUpUserDetailsFragment newInstance(NavigationCallBack navigationCallBack, RegisterModel registerModel) {
        SignUpUserDetailsFragment signUpUserDetailsFragment = new SignUpUserDetailsFragment();
        signUpUserDetailsFragment.mNavigationCallBack = navigationCallBack;
        signUpUserDetailsFragment.mRegisterModel = registerModel;
        return signUpUserDetailsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_user_details, container, false);

        mCityField = (EditText) view.findViewById(R.id.cityField);
        mStatusField = (EditText) view.findViewById(R.id.statusField);
        mConfirmButton = (TextView) view.findViewById(R.id.confirmButton);
        mLanguageSpinner = (Spinner) view.findViewById(R.id.languageSpinner);
        mConfirmButton.setOnClickListener(onConfirm);
        initLanguageSpinner();

        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_user_info), getActivity());
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mNavigationCallBack.backStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void initLanguageSpinner() {

        final ArrayAdapter<CharSequence> dataAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.lang, R.layout.simple_spinner_item);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        mLanguageSpinner.setAdapter(dataAdapter);
        mLanguageSpinner.setSelection(0);
        mLanguageField = dataAdapter.getItem(0).toString();

        mLanguageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLanguageField = dataAdapter.getItem(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private View.OnClickListener onConfirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!isEditTextEmpty(mCityField) && !isEditTextEmpty(mStatusField) && !mLanguageField.isEmpty()) {

                mRegisterModel.person.setStatus(getStringEditText(mStatusField));
                mRegisterModel.person.setTown(getStringEditText(mCityField));
                mRegisterModel.person.setLanguage(mLanguageField);

                HttpRequestAPI api = HttpClient.getInstance();
                api.createUser(mRegisterModel, new Callback<LogInInfo>() {
                    @Override
                    public void success(LogInInfo logInInfo, Response response) {
                        if (!isAttached) return;
                        if (logInInfo.status == 1) {

                            UserDataHelper.saveUserIdToken(getActivity(), logInInfo.userId, logInInfo.token);
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), NavigationActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();

                        } else {
                            DialogFragment dialog = InfoDialog.newInstance(logInInfo.error);
                            dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(retrofitError));
                        dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                    }
                });

            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast_check_input), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private String getStringEditText(EditText edittext) {
        return edittext.getText().toString();
    }

    private boolean isEditTextEmpty(EditText edittext) {
        return edittext.getText().toString().isEmpty();
    }
}
