package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import app.ssteam.photomarket.Adapters.UsersListAdapter;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.UserList;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 20-Apr-15.
 */
public class UserFriendListFragment extends RequestFragment {

    private ListView mFriendsList;
    private UsersListAdapter mAdapter;

    public static UserFriendListFragment newInstance(Bundle bundle) {
        UserFriendListFragment fragment = new UserFriendListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected View setMainView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.fragment_user_friend_list, null);

        mFriendsList = (ListView) rootView.findViewById(R.id.usersList);
        mAdapter = new UsersListAdapter(getActivity());
        mFriendsList.setAdapter(mAdapter);
        mFriendsList.setOnItemClickListener(mAdapter);

        return rootView;
    }

    @Override
    protected String setTitle() {
        return getResources().getString(R.string.action_bar_friends);
    }

    @Override
    protected void onLoad() {
        final Activity activity = getActivity();
        UserEntity user = UserDataHelper.loadUserData(activity);

        HttpRequestAPI api = HttpClient.getInstance();

        api.friendsList(user.token, user.id, getFriendId(), new Callback<UserList>() {

            @Override
            public void success(UserList userList, Response response) {
                if (!isAttached) return;
                mAdapter.setData(UsersListAdapter.EMPTY, userList.content);
            }

            @Override
            public void failure(RetrofitError error) {
                DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(error));
                dialog.show(getActivity().getSupportFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
            }
        });

    }

    private int getFriendId() {
        if (getArguments() != null)
            return getArguments().getInt(UserInfoFragment.FRIEND_ID, -1);
        else return -1;
    }
}
