package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import app.ssteam.photomarket.Adapters.UsersListAdapter;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.UserList;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 21-Apr-15.
 */
public class UserRequestIncomeListFragment extends RequestFragment {

    public static final String IS_SUB_LIST = "Is_sub_list";

    private ListView mIncomeReqList;
    private UsersListAdapter mAdapter;


    public static UserRequestIncomeListFragment newInstance(boolean isSubList, Bundle bundle) {
        UserRequestIncomeListFragment myFragment = new UserRequestIncomeListFragment();
        bundle.putBoolean(IS_SUB_LIST, isSubList);
        myFragment.setArguments(bundle);
        return myFragment;
    }

    public static UserRequestIncomeListFragment newInstance(boolean isSubList) {
        UserRequestIncomeListFragment myFragment = new UserRequestIncomeListFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_SUB_LIST, isSubList);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    protected View setMainView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(R.layout.fragment_req_in_list_fragment, null);

        mIncomeReqList = (ListView) root.findViewById(R.id.incomeReqList);
        mAdapter = new UsersListAdapter(getActivity());

        mIncomeReqList.setAdapter(mAdapter);

        return root;
    }

    @Override
    protected String setTitle() {
        if (!getArguments().getBoolean(IS_SUB_LIST, true)) {
            return getResources().getString(R.string.action_bar_followers);
        }
        return null;
    }

    @Override
    protected void onLoad() {
        final Activity activity = getActivity();
        UserEntity user = UserDataHelper.loadUserData(activity);

        HttpRequestAPI api = HttpClient.getInstance();

        api.incomeRequestList(user.token, user.id, getFriendId(), new Callback<UserList>() {

            @Override
            public void success(UserList userList, Response response) {
                if (!isAttached) return;
                mAdapter.setData(UsersListAdapter.ACCEPT_INCOME, userList.content);
            }

            @Override
            public void failure(RetrofitError error) {
                DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(error));
                dialog.show(getActivity().getSupportFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
            }
        });
    }

    private int getFriendId() {
        if (getArguments() != null)
            return getArguments().getInt(UserInfoFragment.FRIEND_ID, -1);
        else return -1;
    }
}
