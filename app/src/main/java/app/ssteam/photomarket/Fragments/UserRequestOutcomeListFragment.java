package app.ssteam.photomarket.Fragments;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import app.ssteam.photomarket.Adapters.UsersListAdapter;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.RequestList;
import app.ssteam.photomarket.HTTP.Models.UserList;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 20-Apr-15.
 */
public class UserRequestOutcomeListFragment extends RequestFragment {

    private EditText mSearchField;
    private ListView mOutRequestList;
    private UsersListAdapter mAdapter;


    @Override
    protected View setMainView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View root = inflater.inflate(R.layout.fragment_req_out_list_fragment, null);

        mSearchField = (EditText) root.findViewById(R.id.searchField);
        mSearchField.addTextChangedListener(watcher);
        mOutRequestList = (ListView) root.findViewById(R.id.outcomeRequestList);

        mAdapter = new UsersListAdapter(getActivity());
        mOutRequestList.setAdapter(mAdapter);

        return root;
    }

    @Override
    protected String setTitle() {
        return null;
    }

    @Override
    protected void onLoad() {
        final Activity activity = getActivity();
        UserEntity user = UserDataHelper.loadUserData(activity);

        HttpRequestAPI api = HttpClient.getInstance();

        api.requestsPersonList(user.token, user.id, new Callback<RequestList>() {

            @Override
            public void success(RequestList requestList, Response response) {
                if (!isAttached) return;
                mAdapter.setData(UsersListAdapter.REMOVE_OUTCOME, requestList);
            }

            @Override
            public void failure(RetrofitError error) {
                DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(error));
                dialog.show(getActivity().getSupportFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
            }
        });
    }


    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (s.toString().equals("")) {
                mAdapter.clearData();
                onLoad();
                return;
            }

            final Activity activity = getActivity();
            UserEntity user = UserDataHelper.loadUserData(activity);

            HttpRequestAPI api = HttpClient.getInstance();

            api.getUserListByName(user.token, user.id, s.toString(), new Callback<UserList>() {
                @Override
                public void success(UserList userList, Response response) {
                    if (!isAttached) return;
                    if (mSearchField.getText().toString().isEmpty()) {
                        mAdapter.clearData();
                    } else {
                        mAdapter.setData(UsersListAdapter.ADD, userList.content);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    DialogFragment dialog = InfoDialog.newInstance(HttpClient.getErrorMessage(error));
                    dialog.show(getActivity().getSupportFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                }
            });
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

    };
}
