package app.ssteam.photomarket.Fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;

/**
 * Created by Sokol on 8/24/2015.
 */
public class AttachedFragment extends Fragment {

    protected boolean isAttached;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        isAttached = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        isAttached = false;
    }
}
