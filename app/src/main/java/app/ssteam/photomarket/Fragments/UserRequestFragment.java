package app.ssteam.photomarket.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import app.ssteam.photomarket.Adapters.TabAdapter;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 20-Apr-15.
 */
public class UserRequestFragment extends Fragment {

    private TabAdapter mTabAdapter;
    private ViewPager mViewPager;
    private PagerSlidingTabStrip mTabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_user_request_list, null);

        mTabAdapter = new TabAdapter(getChildFragmentManager());
        mTabAdapter.setFragments(new UserRequestOutcomeListFragment(), UserRequestIncomeListFragment.newInstance(true));
        mTabAdapter.setTitles(getResources().getString(R.string.tab_title_outcome), getResources().getString(R.string.tab_title_income));
        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mTabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);

        mViewPager.setAdapter(mTabAdapter);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        mViewPager.setPageMargin(pageMargin);
        mTabs.setViewPager(mViewPager);
        mTabs.setIndicatorColor(getResources().getColor(R.color.action_bar_background));

        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.action_bar_requests));

        return rootView;
    }
}
