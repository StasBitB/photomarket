package app.ssteam.photomarket.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Activities.Interfaces.NavigationCallBack;
import app.ssteam.photomarket.HTTP.Models.RegisterModel;
import app.ssteam.photomarket.HTTP.Models.UserModel;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 12-Jun-15.
 */
public class SignUpUserLoginFragment extends Fragment {

    private EditText mLogin;
    private EditText mPassword;
    private EditText mPasswordConfirmation;
    private TextView mConfirmButton;
    private RegisterModel mRegisterModel;
    private NavigationCallBack mNavigationCallBack;

    public static SignUpUserLoginFragment newInstance(NavigationCallBack navigationCallBack, RegisterModel registerModel) {
        SignUpUserLoginFragment signUpUserLoginFragment = new SignUpUserLoginFragment();
        signUpUserLoginFragment.mNavigationCallBack = navigationCallBack;
        signUpUserLoginFragment.mRegisterModel = registerModel;
        return signUpUserLoginFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up_user_login, container, false);

        mLogin = (EditText) view.findViewById(R.id.loginField);
        mConfirmButton = (TextView) view.findViewById(R.id.confirmButton);
        mPassword = (EditText) view.findViewById(R.id.passwordField);
        mPasswordConfirmation = (EditText) view.findViewById(R.id.passwordConfirmationField);
        mConfirmButton.setOnClickListener(onConfirm);

        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_log_in), getActivity());

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mNavigationCallBack.backStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private View.OnClickListener onConfirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!isEditTextEmpty(mPassword) && !isEditTextEmpty(mPasswordConfirmation) && !isEditTextEmpty(mLogin) &&
                    getStringEditText(mPassword).equals(getStringEditText(mPasswordConfirmation))) {
                mRegisterModel.user = new UserModel(mLogin.getText().toString(), mPassword.getText().toString(), getResources().getString(R.string.mail_test));
                mNavigationCallBack.forwardStack(mRegisterModel);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast_check_input), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private boolean isEditTextEmpty(EditText edittext) {
        return edittext.getText().toString().isEmpty();
    }

    private String getStringEditText(EditText edittext) {
        return edittext.getText().toString();
    }
}
