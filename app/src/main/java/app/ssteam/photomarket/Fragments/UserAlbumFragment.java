package app.ssteam.photomarket.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import app.ssteam.photomarket.Activities.BaseActivity;
import app.ssteam.photomarket.Adapters.AdapterEntities.ImageGridViewItem;
import app.ssteam.photomarket.Adapters.ImagesGridViewAdapter;
import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.ImageItemList;
import app.ssteam.photomarket.HTTP.Models.ImageItem;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 10-Apr-15.
 */
public class UserAlbumFragment extends RequestFragment {

    public static final String ALBUM_ID = "Album_id";

    private GridView mImagesGrid;
    private ImagesGridViewAdapter mAdapter;
    private int mAlbumId;
    private TextView mDescriptionField;

    public static UserAlbumFragment newInstance(int value) {
        UserAlbumFragment myFragment = new UserAlbumFragment();
        Bundle args = new Bundle();
        args.putInt(ALBUM_ID, value);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    protected View setMainView() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(R.layout.fragment_user_album, null);
        mImagesGrid = (GridView) root.findViewById(R.id.imagesGrid);
        mDescriptionField = (TextView) root.findViewById(R.id.descriptionField);
        mAlbumId = getArguments().getInt(ALBUM_ID, 0);

        mAdapter = new ImagesGridViewAdapter(getActivity(), new ArrayList<ImageGridViewItem>());

        mImagesGrid.setAdapter(mAdapter);
        BaseActivity.initActionBar(getResources().getString(R.string.action_bar_albums), getActivity());
        return root;
    }

    @Override
    protected String setTitle() {
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onLoad() {
        showLoading();

        UserEntity user = UserDataHelper.loadUserData(getActivity());

        HttpRequestAPI api = HttpClient.getInstance();
        api.getAlbumImageList(user.token, user.id, mAlbumId, new Callback<ImageItemList>() {

            @Override
            public void success(ImageItemList imageItemList, Response response) {
                if (!isAttached) return;
                BaseActivity.initActionBar(imageItemList.album_title, getActivity());
                ArrayList<ImageGridViewItem> data = new ArrayList<>();
                for (int j = 0; j < imageItemList.content.size(); j++) {
                    ImageItem item = imageItemList.content.get(j);
                    data.add(new ImageGridViewItem(item.image.name, item.image.url));
                }
                mDescriptionField.setText(imageItemList.album_description);
                mAdapter.setData(data);
                showView();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                showError();
                DialogFragment dialog = InfoDialog.newInstance(retrofitError.getMessage());
                dialog.show(getFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
            }
        });

    }
}
