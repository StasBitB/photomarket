package app.ssteam.photomarket.HTTP;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedByteArray;

/**
 * Created by Stas on 27-Mar-15.
 */
public class HttpClient {

//    private final static String SERVER_URL_API = "http://192.168.189.1:8000/api";
//    private final static String SERVER_URL = "http://192.168.189.1:8000";

    private final static String SERVER_URL_API = "http://staspa.pythonanywhere.com/api";
    private final static String SERVER_URL = "http://staspa.pythonanywhere.com/";

    public static String get_SERVER_URL() {
        return SERVER_URL;
    }

    private static HttpRequestAPI instance;

    public static synchronized HttpRequestAPI getInstance() {
        if (instance == null) {

            Gson gson = new GsonBuilder()
                    .create();

            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint(SERVER_URL_API)
                    .setConverter(new GsonConverter(gson))
                    .build();
            instance = adapter.create(HttpRequestAPI.class);
        }
        return instance;
    }

    public static String getErrorMessage(RetrofitError error) {
        if (error != null && error.getBody() != null) {
            String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
            String[] res = json.split("Exception Value:", 3);
            if (res.length > 2) {
                return res[2].split("\\.")[0];
            } else return error.getMessage();
        } else return error.getMessage();

    }

    private HttpClient() {
    }


}
