package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 20-Jun-15.
 */
public class PersonModel implements Serializable {

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    private String status;
    private String town;
    private String language;
    private String first_name;
    private String last_name;

    public PersonModel() {
        this.status = "";
        this.town = "";
        this.language = "";
        this.first_name = "";
        this.last_name = "";
    }
}
