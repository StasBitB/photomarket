package app.ssteam.photomarket.HTTP;

import java.util.ArrayList;

import app.ssteam.photomarket.HTTP.Models.FriendShip;
import app.ssteam.photomarket.HTTP.Models.ImageItemList;
import app.ssteam.photomarket.HTTP.Models.LogInInfo;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.HTTP.Models.RegisterModel;
import app.ssteam.photomarket.HTTP.Models.Request;
import app.ssteam.photomarket.HTTP.Models.RequestList;
import app.ssteam.photomarket.HTTP.Models.UserAlbum;
import app.ssteam.photomarket.HTTP.Models.UserAlbumList;
import app.ssteam.photomarket.HTTP.Models.UserInformation;
import app.ssteam.photomarket.HTTP.Models.UserList;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.FormUrlEncodedTypedOutput;
import retrofit.mime.TypedFile;

/**
 * Created by Stas on 27-Mar-15.
 */
public interface HttpRequestAPI {

    @Headers("Content-Type: application/json")
    @POST("/register/")
    void createUser(@Body RegisterModel model, Callback<LogInInfo> callback);

    @FormUrlEncoded
    @POST("/login/")
    void LoginUser(@Field("userName") String username, @Field("password") String password, Callback<LogInInfo> callback);

    @GET("/user_information/")
    void getUserInformation(@Header("Authorization") String authorization, @Query("personId") int id, @Query("personInfoId") int userId, Callback<UserInformation> callback);

    @GET("/get_user_by_name/")
    void getUserListByName(@Header("Authorization") String authorization, @Query("personId") int id,
                           @Query("requestedName") String name, Callback<UserList> callback);

    @FormUrlEncoded
    @POST("/set_person_info/")
    void setUserInformation(@Header("Authorization") String authorization, @Field("personId") int person_id, @Field("photoId") int photo_id, @Field("firstName") String first_name,
                            @Field("secondName") String second_name, @Field("language") String language, @Field("town") String town, @Field("status") String status, Callback<UserInformation> callback);

    @GET("/get_albums/")
    void getUserAlbums(@Header("Authorization") String authorization, @Query("personId") int id, @Query("personInfoId") int userId, Callback<UserAlbumList> callback);


    @FormUrlEncoded
    @POST("/create_album/")
    void createNewAlbum(@Header("Authorization") String authorization, @Field("personId") int id, @Field("title") String title, @Field("description") String description, @Field("images") ArrayList<Integer> images, Callback<UserAlbum> callback);

    @FormUrlEncoded
    @POST("/request_person/")
    void requestsPerson(@Header("Authorization") String authorization, @Field("personId") int personId, @Field("requestedPersonId") int requestedPersonId, Callback<Request> callback);

    @FormUrlEncoded
    @POST("/request_person_remove/")
    void requestsPersonDelete(@Header("Authorization") String authorization, @Field("personId") int personId, @Field("requestedPersonId") int requestedPersonId, Callback<Request> callback);

    @FormUrlEncoded
    @POST("/income_request_accept/")
    void incomeRequestAccept(@Header("Authorization") String authorization, @Field("personId") int personId, @Field("requestedPersonId") int requestedPersonId, Callback<FriendShip> callback);

    @GET("/request_person_list/")
    void requestsPersonList(@Header("Authorization") String authorization, @Query("personId") int personId, Callback<RequestList> callback);

    @GET("/income_request_list/")
    void incomeRequestList(@Header("Authorization") String authorization, @Query("personId") int personId, @Query("personInfoId") int userId, Callback<UserList> callback);

    @GET("/get_friends_list/")
    void friendsList(@Header("Authorization") String authorization, @Query("personId") int personId, @Query("personInfoId") int userId, Callback<UserList> callback);

    @Multipart
    @POST("/upload_image/")
    PhotoModel uploadImageS(@Part("image_file") TypedFile file, @Header("Authorization") String authorization, @Part("info") FormUrlEncodedTypedOutput output);


    @Multipart
    @POST("/upload_photo/")
    PhotoModel uploadUserPhotoSync(@Part("image_file") TypedFile file, @Part("info") FormUrlEncodedTypedOutput output);

    @GET("/get_album_image_list/")
    void getAlbumImageList(@Header("Authorization") String authorization, @Query("personId") int personId, @Query("albumId") int albumId, Callback<ImageItemList> callback);
}
