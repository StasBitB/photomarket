package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;
import java.util.ArrayList;

import app.ssteam.photomarket.HTTP.Models.ImageItem;

/**
 * Created by Stas on 10-Apr-15.
 */
public class ImageItemList implements Serializable {

    public int user_id;
    public int album_id;
    public String album_title;
    public String album_description;
    public ArrayList<ImageItem> content;
}
