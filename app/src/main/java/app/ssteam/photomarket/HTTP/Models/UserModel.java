package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 20-Jun-15.
 */
public class UserModel implements Serializable {

    public String username;
    public String password;
    public String email;

    public UserModel(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
}


