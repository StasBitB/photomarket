package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 27-Mar-15.
 */
public class LogInInfo implements Serializable{
    public int status;
    public String error;
    public String token;
    public String userName;
    public int userId;
}
