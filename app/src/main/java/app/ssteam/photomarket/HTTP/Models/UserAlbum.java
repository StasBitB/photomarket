package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 08-Apr-15.
 */
public class UserAlbum implements Serializable {
    public int id;
    public PhotoModel icon;
    public String title;
    public String description;
}
