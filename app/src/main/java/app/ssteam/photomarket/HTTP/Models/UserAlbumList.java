package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Stas on 08-Apr-15.
 */
public class UserAlbumList implements Serializable {
    public int user_id;
    public ArrayList<UserAlbum> content;
}
