package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Stas on 24-Apr-15.
 */
public class RequestList implements Serializable {
    public int user_id;
    public ArrayList<Request> content;

}
