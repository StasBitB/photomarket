package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Stas on 22-Apr-15.
 */
public class UserList implements Serializable {
    public int user_id;
    public ArrayList<UserInformation> content;
}
