package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 20-Jun-15.
 */
public class PhotoModel implements Serializable{
    public int id;
    public String name;
    public String height;
    public String width;
    public String url;

    public PhotoModel(int id, String name, String height, String width, String url) {
        this.id = id;
        this.name = name;
        this.height = height;
        this.width = width;
        this.url = url;
    }
}
