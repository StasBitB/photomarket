package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 20-Jun-15.
 */
public class RegisterModel implements Serializable {
    public UserModel user;
    public PersonModel person;
    public PhotoModel photo;
}
