package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 30-Mar-15.
 */
public class UserInformation implements Serializable {
    public String first_name;
    public int id;
    public String last_name;
    public User user;
    public String status;
    public String town;
    public String language;
    public PhotoModel photo;
}
