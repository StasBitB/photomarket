package app.ssteam.photomarket.HTTP.Models;

import java.io.Serializable;

/**
 * Created by Stas on 27-Mar-15.
 */
public class User implements Serializable {
    public String first_name;
    public String last_nam;
    public String email;
    public String is_active;
}
