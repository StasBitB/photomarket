package app.ssteam.photomarket.Tasks;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.Helper.FileHelper;
import app.ssteam.photomarket.Tasks.CallBacks.FragmentCallback;
import retrofit.mime.FormUrlEncodedTypedOutput;
import retrofit.mime.TypedFile;

/**
 * Created by Sokol on 7/3/2015.
 */
public class CropUserPhotoTask extends AsyncTask<Uri, Void, PhotoModel> {

    private FragmentCallback mFragmentCallback;
    private Context mContext;

    public CropUserPhotoTask(FragmentCallback mFragmentCallback, Context mContext) {
        this.mFragmentCallback = mFragmentCallback;
        this.mContext = mContext;
    }

    protected PhotoModel doInBackground(Uri... uris) {
        String path = FileHelper.getRealPathFromURI(mContext.getContentResolver(), uris[0]);
        String fileName = path.substring(path.lastIndexOf("/") + 1).split("\\.")[0];
        String tempPath = new ContextWrapper(mContext).getFilesDir().getPath() + "/" + fileName + ".png";
        int targetW = 800;
        int targetH = 400;
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap roughBitmap = BitmapFactory.decodeFile(path, bmOptions);

        Matrix m = new Matrix();
        RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
        RectF outRect = new RectF(0, 0, targetW, targetH);
        m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
        float[] values = new float[9];
        m.getValues(values);

        // resize bitmap
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);

        try {

            FileOutputStream out = new FileOutputStream(tempPath);
            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
        } catch (Exception e) {
            Log.e("Image", e.getMessage(), e);
        }

        File photo = new File(tempPath);
        TypedFile image = new TypedFile("multipart/form-data", photo);

        FormUrlEncodedTypedOutput form = new FormUrlEncodedTypedOutput();
        form.addField("name", String.valueOf(fileName));
        form.addField("height", String.valueOf(targetH));
        form.addField("width", String.valueOf(targetW));


        HttpRequestAPI api = HttpClient.getInstance();
        PhotoModel model = api.uploadUserPhotoSync(image, form);
        return model;
    }

    protected void onPostExecute(PhotoModel model) {
        mFragmentCallback.onTaskDone(model);
    }
}
