package app.ssteam.photomarket.Tasks.CallBacks;

import app.ssteam.photomarket.HTTP.Models.PhotoModel;

/**
 * Created by Sokol on 7/7/2015.
 */
public interface FragmentCallback {
    public void onTaskDone(PhotoModel model);
}
