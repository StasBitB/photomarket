package app.ssteam.photomarket.Helper;

/**
 * Created by Stas on 18-Apr-15.
 */
public class UserEntity {
    public int id;
    public String token;
    public String photoUrl;

    UserEntity(int id, String token, String photoUrl) {
        this.id = id;
        this.photoUrl = photoUrl;
        this.token = token;
    }

}
