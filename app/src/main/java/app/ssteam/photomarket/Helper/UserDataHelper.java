package app.ssteam.photomarket.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import java.io.FileOutputStream;

import app.ssteam.photomarket.HTTP.Models.UserInformation;

/**
 * Created by Stas on 18-Apr-15.
 */
public class UserDataHelper {

    public static void saveUserPhotoUrl(Activity activity, String url) {
        SharedPreferences sharedPref = activity.getSharedPreferences(SharedPreferencesConstants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SharedPreferencesConstants.USER_PHOTO, url);
        editor.commit();
    }

    public static void saveUserIdToken(Activity activity, int userId, String token) {
        SharedPreferences sharedPref = activity.getSharedPreferences(SharedPreferencesConstants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(SharedPreferencesConstants.USER_ID, userId);
        editor.putString(SharedPreferencesConstants.USER_TOKEN, token);
        editor.commit();
    }

    public static UserEntity loadUserData(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SharedPreferencesConstants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        int id = sharedPref.getInt(SharedPreferencesConstants.USER_ID, -1);
        String token = "Token " + sharedPref.getString(SharedPreferencesConstants.USER_TOKEN, "");
        String photoUrl = sharedPref.getString(SharedPreferencesConstants.USER_PHOTO, "");
        return new UserEntity(id, token, photoUrl);
    }

    public static void saveUserObject(Activity activity,UserInformation userInformation){
        SharedPreferences sharedPref = activity.getSharedPreferences(SharedPreferencesConstants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        editor.putString(SharedPreferencesConstants.USER_OBJECT, gson.toJson(userInformation));
        editor.commit();
    }

    public static UserInformation loadUserObject(Activity activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences(SharedPreferencesConstants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        JsonParser parser=new JsonParser();
        String toParse = sharedPref.getString(SharedPreferencesConstants.USER_OBJECT, "");
        return gson.fromJson(parser.parse(toParse).getAsJsonObject(), UserInformation.class);
    }

}
