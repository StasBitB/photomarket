package app.ssteam.photomarket.Helper;

/**
 * Created by Stas on 30-Mar-15.
 */
public class SharedPreferencesConstants {
    public static final String PREFERENCES_NAME = "myApp";

    public static final String USER_ID = "userId";
    public static final String USER_TOKEN = "userToken";
    public static final String USER_PHOTO = "userPhotoUrl";
    public static final String USER_OBJECT = "userObject";
}
