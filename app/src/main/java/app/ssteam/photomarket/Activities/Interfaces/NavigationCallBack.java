package app.ssteam.photomarket.Activities.Interfaces;

import app.ssteam.photomarket.HTTP.Models.RegisterModel;

/**
 * Created by Stas on 20-Jun-15.
 */
public interface NavigationCallBack {

    void forwardStack(RegisterModel registerModel);
    void backStack();
}
