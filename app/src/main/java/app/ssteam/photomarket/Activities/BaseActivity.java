package app.ssteam.photomarket.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

import app.ssteam.photomarket.R;

/**
 * Created by Stas on 23-Mar-15.
 */
public abstract class BaseActivity extends ActionBarActivity {

    protected abstract Fragment setFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
        if (fragment == null) {
            fragment = setFragment();
            fragmentManager.beginTransaction().add(R.id.fragment, fragment).commit();
        }
    }


    public static void initActionBar(String title, Activity activity) {
        ActionBarActivity actionBarActivity = (ActionBarActivity) activity;
        actionBarActivity.getSupportActionBar().setTitle(title);
        actionBarActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        actionBarActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
