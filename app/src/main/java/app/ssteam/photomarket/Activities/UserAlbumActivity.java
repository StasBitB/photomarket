package app.ssteam.photomarket.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import app.ssteam.photomarket.Fragments.UserAlbumFragment;

/**
 * Created by Stas on 10-Apr-15.
 */
public class UserAlbumActivity extends BaseActivity {
    @Override
    protected Fragment setFragment() {
        Bundle extras = getIntent().getExtras();
        int albumId = 0;
        if (extras != null) {
            albumId = extras.getInt(UserAlbumFragment.ALBUM_ID);
        }
        return UserAlbumFragment.newInstance(albumId);
    }
}
