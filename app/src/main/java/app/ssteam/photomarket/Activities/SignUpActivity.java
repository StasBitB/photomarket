package app.ssteam.photomarket.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.widget.ProgressBar;

import app.ssteam.photomarket.Activities.Interfaces.NavigationCallBack;
import app.ssteam.photomarket.Fragments.SignUpUserDetailsFragment;
import app.ssteam.photomarket.Fragments.SignUpUserInfoFragment;
import app.ssteam.photomarket.Fragments.SignUpUserLoginFragment;
import app.ssteam.photomarket.HTTP.Models.RegisterModel;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 23-Mar-15.
 */
public class SignUpActivity extends ActionBarActivity {

    public final static int USER_INFO_FRAGMENT_NUMBER = 1;
    public final static int USER_LOGIN_FRAGMENT_NUMBER = 2;
    public final static int USER_DETAILS_FRAGMENT_NUMBER = 3;

    private ProgressBar mProgressBar;
    private int mCurrentFragmentNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mCurrentFragmentNumber = USER_INFO_FRAGMENT_NUMBER;
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
        if (fragment == null) {
            fragment = SignUpUserInfoFragment.newInstance(navigationCallBack);
            fragmentManager.beginTransaction().add(R.id.fragment, fragment).commit();
        }
    }


    private NavigationCallBack navigationCallBack = new NavigationCallBack() {
        @Override
        public void forwardStack(RegisterModel registerModel) {
            mCurrentFragmentNumber++;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Fragment newFragment = null;
            switch (mCurrentFragmentNumber) {
                case USER_LOGIN_FRAGMENT_NUMBER:
                    newFragment = SignUpUserLoginFragment.newInstance(navigationCallBack, registerModel);
                    break;
                case USER_DETAILS_FRAGMENT_NUMBER:
                    newFragment = SignUpUserDetailsFragment.newInstance(navigationCallBack, registerModel);
                    break;
            }
            transaction.replace(R.id.fragment, newFragment).addToBackStack(null).commit();
            mProgressBar.setProgress(mCurrentFragmentNumber);
        }

        @Override
        public void backStack() {
            mCurrentFragmentNumber--;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack();
            mProgressBar.setProgress(mCurrentFragmentNumber);
        }
    };

}
