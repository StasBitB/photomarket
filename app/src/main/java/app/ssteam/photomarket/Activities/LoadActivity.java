package app.ssteam.photomarket.Activities;


import android.support.v4.app.Fragment;

import app.ssteam.photomarket.Fragments.LoadFragment;

/**
 * Created by Stas on 23-Mar-15.
 */
public class LoadActivity extends BaseActivity {

    @Override
    protected Fragment setFragment() {
        return new LoadFragment();
    }
}
