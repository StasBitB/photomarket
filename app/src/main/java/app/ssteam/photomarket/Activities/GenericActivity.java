package app.ssteam.photomarket.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

import app.ssteam.photomarket.Fragments.UserAlbumListFragment;
import app.ssteam.photomarket.Fragments.UserFriendListFragment;
import app.ssteam.photomarket.Fragments.UserInfoFragment;
import app.ssteam.photomarket.Fragments.UserRequestIncomeListFragment;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 04-May-15.
 */
public class GenericActivity extends ActionBarActivity {

    public final static String FRAGMENT_KEY = "Fragment";

    public final static String FRIEND_LIST_FRAGMENT = "FriendListFragment";
    public final static String REQUEST_INCOME_LIST_FRAGMENT = "FollowerListFragment";
    public final static String ALBUM_LIST_FRAGMENT = "AlbumListFragment";
    public final static String INFO_FRAGMENT = "UserInfoFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        String fragmentName = getIntent().getExtras().getString(FRAGMENT_KEY);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
        Fragment view;
        if (fragment == null) {
            switch (fragmentName) {
                case FRIEND_LIST_FRAGMENT:
                    view = UserFriendListFragment.newInstance(getIntent().getExtras());
                    break;
                case REQUEST_INCOME_LIST_FRAGMENT:
                    view = UserRequestIncomeListFragment.newInstance(false, getIntent().getExtras());
                    break;
                case ALBUM_LIST_FRAGMENT:
                    view = UserAlbumListFragment.newInstance(getIntent().getExtras());
                    break;
                case INFO_FRAGMENT:
                    view = UserInfoFragment.newInstance(getIntent().getExtras());
                    break;
                default:
                    view = new UserInfoFragment();
                    break;
            }
            fragmentManager.beginTransaction().add(R.id.fragment, view).commit();
        }
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
}
