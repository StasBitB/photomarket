package app.ssteam.photomarket.Activities;


import android.support.v4.app.Fragment;

import app.ssteam.photomarket.Fragments.LoginFragment;

/**
 * Created by Stas on 23-Mar-15.
 */
public class LoginActivity extends BaseActivity {

    @Override
    protected Fragment setFragment() {
        return new LoginFragment();
    }

}
