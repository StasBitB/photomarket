package app.ssteam.photomarket.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import app.ssteam.photomarket.Dialogs.InfoDialog;
import app.ssteam.photomarket.Fragments.LeftMenuFragment;
import app.ssteam.photomarket.Fragments.UserAlbumListFragment;
import app.ssteam.photomarket.Fragments.UserFriendListFragment;
import app.ssteam.photomarket.Fragments.UserInfoFragment;
import app.ssteam.photomarket.Fragments.UserRequestFragment;
import app.ssteam.photomarket.Helper.SharedPreferencesConstants;
import app.ssteam.photomarket.R;

public class NavigationActivity extends ActionBarActivity
        implements LeftMenuFragment.NavigationDrawerCallbacks {


    private LeftMenuFragment mNavigationDrawerFragment;

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        mNavigationDrawerFragment = (LeftMenuFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer, toolbar,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        mActivity = this;
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new UserInfoFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case 1:
                fragment = new UserAlbumListFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case 2:
                fragment = new UserFriendListFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case 3:
                fragment = new UserRequestFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case 4:
                DialogFragment dialog = InfoDialog.newInstance(getResources().getString(R.string.dialog_logout), logOutCallBack);
                dialog.show(getSupportFragmentManager(), InfoDialog.INFO_DIALOG_TAG);
                break;
            default:
                fragment = new UserInfoFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
        }

    }

    public interface LogOut {
        void logOut();
    }

    private LogOut logOutCallBack = new LogOut() {

        @Override
        public void logOut() {
            SharedPreferences settings = getSharedPreferences(SharedPreferencesConstants.PREFERENCES_NAME, Context.MODE_PRIVATE);
            settings.edit().clear().commit();
            Intent intent = new Intent();
            intent.setClass(mActivity, LoadActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            mActivity.finish();
        }
    };
}
