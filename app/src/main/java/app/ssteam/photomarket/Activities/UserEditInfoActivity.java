package app.ssteam.photomarket.Activities;

import android.support.v4.app.Fragment;

import app.ssteam.photomarket.Fragments.UserInfoEditFragment;

/**
 * Created by Stas on 13-Apr-15.
 */
public class UserEditInfoActivity extends BaseActivity {

    @Override
    protected Fragment setFragment() {
        return new UserInfoEditFragment();
    }
}
