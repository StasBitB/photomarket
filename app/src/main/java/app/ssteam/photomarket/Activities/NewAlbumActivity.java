package app.ssteam.photomarket.Activities;


import android.support.v4.app.Fragment;

import app.ssteam.photomarket.Fragments.UserAlbumNewFragment;

/**
 * Created by Stas on 07-Apr-15.
 */
public class NewAlbumActivity extends BaseActivity {


    @Override
    protected Fragment setFragment() {
        return new UserAlbumNewFragment();
    }
}
