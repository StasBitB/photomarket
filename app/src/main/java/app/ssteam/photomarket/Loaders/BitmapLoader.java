package app.ssteam.photomarket.Loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;

import java.io.FileNotFoundException;

import app.ssteam.photomarket.Helper.BitmapHelper;

/**
 * Created by Sokol on 8/25/2015.
 */
public class BitmapLoader extends AsyncTaskLoader<Bitmap> {

    private Context mContext;
    private Uri mUri;

    public BitmapLoader(Context context, Uri uri) {
        super(context);
        mContext = context;
        mUri = uri;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }


    @Override
    protected void onReset() {
        super.onReset();
        mContext = null;
        mUri = null;
    }

    @Override
    public Bitmap loadInBackground() {

        try {
            return BitmapHelper.decodeUri(mContext, mUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
