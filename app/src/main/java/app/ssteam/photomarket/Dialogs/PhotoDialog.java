package app.ssteam.photomarket.Dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import app.ssteam.photomarket.R;

/**
 * Created by Sokol on 8/2/2015.
 */
public class PhotoDialog extends DialogFragment {

    public static final String PHOTO_DIALOG_TAG = "Photo_Dialog";
    public static final int CAMERA = 1;
    public static final int GALLERY = 2;

    private DialogResult mDialogResult;
    private TextView mCameraButton;
    private TextView mGalleryPhoto;

    public static PhotoDialog newInstance(DialogResult dialogResult) {
        PhotoDialog f = new PhotoDialog();
        f.mDialogResult = dialogResult;
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_photo, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        mCameraButton = (TextView) v.findViewById(R.id.cameraButton);
        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogResult.result(CAMERA);
                dismiss();
            }
        });
        mGalleryPhoto = (TextView) v.findViewById(R.id.galleryButton);
        mGalleryPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogResult.result(GALLERY);
                dismiss();
            }
        });

        return v;
    }


    public interface DialogResult {
        void result(int source);
    }
}
