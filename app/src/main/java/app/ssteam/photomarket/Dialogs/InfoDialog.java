package app.ssteam.photomarket.Dialogs;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import app.ssteam.photomarket.Activities.NavigationActivity;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 27-Mar-15.
 */
public class InfoDialog extends DialogFragment {

    public static final String INFO_DIALOG_TAG = "Info_Dialog";
    private final static String ARGUMENTS = "Arguments";

    public NavigationActivity.LogOut callBack;
    private TextView mInfoField;
    private TextView mOkButton;

    public static InfoDialog newInstance(String info) {
        InfoDialog f = new InfoDialog();
        Bundle args = new Bundle();
        args.putString(ARGUMENTS, info);
        f.setArguments(args);
        return f;
    }

    public static InfoDialog newInstance(String info, NavigationActivity.LogOut callBack) {
        InfoDialog f = new InfoDialog();
        f.callBack = callBack;
        Bundle args = new Bundle();
        args.putString(ARGUMENTS, info);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_info, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mOkButton = (TextView) v.findViewById(R.id.okButton);
        mInfoField = (TextView) v.findViewById(R.id.infoField);

        Bundle bundle = this.getArguments();
        String info = bundle.getString(ARGUMENTS, "");
        mInfoField.setText(info);
        mOkButton.setOnClickListener(okButtonListener);

        return v;
    }

    View.OnClickListener okButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (callBack != null) {
                callBack.logOut();
            }
            dismiss();
        }
    };


}
