package app.ssteam.photomarket.Dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import app.ssteam.photomarket.R;

/**
 * Created by Stas on 20-Apr-15.
 */
public class ProgressDialog extends DialogFragment {

    public static final String PROGRESS_DIALOG_TAG = "Progress_Dialog";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_progress, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return v;
    }


}
