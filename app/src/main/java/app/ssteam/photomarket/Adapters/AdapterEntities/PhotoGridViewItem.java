package app.ssteam.photomarket.Adapters.AdapterEntities;

import android.graphics.Bitmap;

import java.util.UUID;

import app.ssteam.photomarket.HTTP.Models.PhotoModel;

/**
 * Created by Stas on 08-Apr-15.
 */
public class PhotoGridViewItem {

    public Bitmap bitmap;
    public String title;
    public boolean isUploaded;
    public UUID uuid;
    public PhotoModel photoModel;

    public PhotoGridViewItem(Bitmap bitmap, String tittle) {
        uuid = UUID.randomUUID();
        this.bitmap = bitmap;
        this.title = tittle;
        isUploaded = false;
        this.photoModel = null;
    }

    public PhotoGridViewItem(Bitmap bitmap, String tittle,PhotoModel photoModel) {
        uuid = UUID.randomUUID();
        this.bitmap = bitmap;
        this.title = tittle;
        this.isUploaded = true;
        this.photoModel = photoModel;
    }
}
