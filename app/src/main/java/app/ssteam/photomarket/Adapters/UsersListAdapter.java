package app.ssteam.photomarket.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;

import app.ssteam.photomarket.Activities.GenericActivity;
import app.ssteam.photomarket.Fragments.UserInfoFragment;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.FriendShip;
import app.ssteam.photomarket.HTTP.Models.Request;
import app.ssteam.photomarket.HTTP.Models.RequestList;
import app.ssteam.photomarket.HTTP.Models.UserInformation;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import app.ssteam.photomarket.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Stas on 21-Apr-15.
 */
public class UsersListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    public static final int ADD = 1;
    public static final int REMOVE_OUTCOME = 2;
    public static final int ACCEPT_INCOME = 3;
    public static final int EMPTY = 4;

    private Activity mContext;
    private ArrayList<UserInformation> users;
    private int currentState;

    public UsersListAdapter(Activity activity) {
        mContext = activity;
        users = new ArrayList<>();
    }

    public void setData(int currentState, ArrayList<UserInformation> users) {
        this.users = users;
        this.currentState = currentState;
        notifyDataSetChanged();
    }

    public void setData(int currentState, RequestList content) {
        users = new ArrayList<>();
        for (Request request : content.content) {
            users.add(request.requested_person);
        }
        this.currentState = currentState;
        notifyDataSetChanged();
    }

    public void clearData() {
        this.users = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.user_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.nameField);
            viewHolder.addButton = (TextView) convertView.findViewById(R.id.addButton);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.imageField);
            convertView.setTag(viewHolder);
        }

        holder = (ViewHolder) convertView.getTag();

        final UserInformation requested = users.get(position);

        holder.text.setText(requested.first_name + "  " + requested.last_name);
        String url = HttpClient.get_SERVER_URL() + requested.photo.url;
        Picasso.with(mContext).load(url).resize(100, 100).centerCrop().into(holder.icon);

        if (currentState == ADD) {
            holder.addButton.setText(mContext.getResources().getString(R.string.list_button_add));
            holder.addButton.setOnClickListener(onClickAdd(requested.id));
        } else if (currentState == REMOVE_OUTCOME) {
            holder.addButton.setText(mContext.getResources().getString(R.string.list_button_rem));
            holder.addButton.setOnClickListener(onClickRemoveOutcome(requested.id));
        } else if (currentState == ACCEPT_INCOME) {
            holder.addButton.setText(mContext.getResources().getString(R.string.list_button_con));
            holder.addButton.setOnClickListener(onClickAcceptIncome(requested.id));
        }else if (currentState == EMPTY) {
            holder.addButton.setVisibility(View.GONE);
        }


        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserInformation clicked = users.get(position);
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(GenericActivity.FRAGMENT_KEY, GenericActivity.INFO_FRAGMENT);
        bundle.putInt(UserInfoFragment.FRIEND_ID, clicked.id);
        intent.putExtras(bundle);
        intent.setClass(mContext, GenericActivity.class);
        mContext.startActivity(intent);
    }

    private View.OnClickListener onClickAdd(final int id) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserEntity user = UserDataHelper.loadUserData(mContext);

                HttpRequestAPI api = HttpClient.getInstance();

                api.requestsPerson(user.token, user.id, id, new Callback<Request>() {

                    @Override
                    public void success(Request request, Response response) {
                        Iterator<UserInformation> iter = users.iterator();
                        while (iter.hasNext()) {
                            UserInformation information = iter.next();
                            if (information.id == request.requested_person.id)
                                iter.remove();
                        }
                        notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        };
    }

    private View.OnClickListener onClickRemoveOutcome(final int id) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserEntity user = UserDataHelper.loadUserData(mContext);

                HttpRequestAPI api = HttpClient.getInstance();

                api.requestsPersonDelete(user.token, user.id, id, new Callback<Request>() {

                    @Override
                    public void success(Request request, Response response) {
                        Iterator<UserInformation> iter = users.iterator();
                        while (iter.hasNext()) {
                            UserInformation information = iter.next();
                            if (information.id == request.requested_person.id)
                                iter.remove();
                        }
                        notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        };
    }

    private View.OnClickListener onClickAcceptIncome(final int id) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserEntity user = UserDataHelper.loadUserData(mContext);

                HttpRequestAPI api = HttpClient.getInstance();

                api.incomeRequestAccept(user.token, user.id, id, new Callback<FriendShip>() {

                    @Override
                    public void success(FriendShip friendShip, Response response) {
                        Iterator<UserInformation> iter = users.iterator();
                        while (iter.hasNext()) {
                            UserInformation information = iter.next();
                            if (information.id == friendShip.friend.id)
                                iter.remove();
                        }
                        notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        };
    }

    static class ViewHolder {
        TextView text;
        TextView addButton;
        ImageView icon;
    }
}
