package app.ssteam.photomarket.Adapters.AdapterEntities;


/**
 * Created by Stas on 10-Apr-15.
 */
public class ImageGridViewItem {

    public String name;
    public String url;
    public boolean isUploaded;

    public ImageGridViewItem(String name, String url) {
        this.name = name;
        this.url = url;
        isUploaded = false;
    }
}
