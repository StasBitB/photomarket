package app.ssteam.photomarket.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import app.ssteam.photomarket.Adapters.AdapterEntities.PhotoGridViewItem;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 08-Apr-15.
 */
public class PhotoGridViewAdapter extends BaseAdapter {

    private Context mContext;
    private boolean mIsUploadedSet;
    private ArrayList<PhotoGridViewItem> mPhotosList;

    public PhotoGridViewAdapter(Context context) {
        this.mContext = context;
        this.mIsUploadedSet = false;
        this.mPhotosList = new ArrayList<>();
    }

    public void addPhotoToUpload(PhotoGridViewItem gridViewPhotoItem) {
        mPhotosList.add(gridViewPhotoItem);
        notifyDataSetChanged();
    }

    public void addUploaded(List<PhotoModel> list) {
        if (!mIsUploadedSet) {
            for (PhotoModel photoModel : list) {
                mPhotosList.add(new PhotoGridViewItem(null, photoModel.name, photoModel));
            }
            mIsUploadedSet = true;
        }
    }

    public void setUploaded(UUID uuid, PhotoModel model) {
        for (PhotoGridViewItem item : mPhotosList) {
            if (item.uuid.equals(uuid)) {
                item.isUploaded = true;
                item.photoModel = model;
                break;
            }
        }
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getUploadedIds() {
        ArrayList<Integer> ids = new ArrayList<>();
        for (PhotoGridViewItem image : mPhotosList) {
            if (image.photoModel != null)
                ids.add(image.photoModel.id);
        }
        return ids;
    }

    @Override
    public int getCount() {
        return mPhotosList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPhotosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.album_thumb_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.titleAlbumField);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.thumbAlbumField);
            viewHolder.bar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            convertView.setTag(viewHolder);
        }

        holder = (ViewHolder) convertView.getTag();

        PhotoGridViewItem item = mPhotosList.get(position);
        holder.text.setText(item.title);
        if (item.bitmap == null) {
            String url = HttpClient.get_SERVER_URL() + item.photoModel.url;
            Picasso.with(mContext).load(url).resize(350, 350).centerCrop().into(holder.icon);
        } else {
            holder.icon.setImageBitmap(item.bitmap);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView text;
        ProgressBar bar;
        ImageView icon;
    }
}
