package app.ssteam.photomarket.Adapters.AdapterEntities;

/**
 * Created by Stas on 08-Apr-15.
 */
public class AlbumGridViewItem {

    public int id;
    public String title;
    public String description;
    public String iconUrl;

    public AlbumGridViewItem(int id, String title, String description, String iconUrl) {
        this.id = id;
        this.iconUrl =iconUrl;
        this.description = description;
        this.title = title;
    }
}
