package app.ssteam.photomarket.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Stas on 20-Apr-15.
 */
public class TabAdapter extends FragmentPagerAdapter {

    private Fragment[] mFragmentList;
    private String[] mTitlesList;

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setFragments(Fragment... fragmentList) {
        mFragmentList = fragmentList;
        notifyDataSetChanged();
    }

    public void setTitles(String... titlesList) {
        mTitlesList = titlesList;
        notifyDataSetChanged();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitlesList[position];
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList[position];
    }

    @Override
    public int getCount() {
        return mTitlesList.length;
    }
}
