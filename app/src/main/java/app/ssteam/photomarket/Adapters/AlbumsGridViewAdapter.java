package app.ssteam.photomarket.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import app.ssteam.photomarket.Activities.UserAlbumActivity;
import app.ssteam.photomarket.Adapters.AdapterEntities.AlbumGridViewItem;
import app.ssteam.photomarket.Adapters.AdapterEntities.ImageGridViewItem;
import app.ssteam.photomarket.Fragments.UserAlbumFragment;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 07-Apr-15.
 */
public class AlbumsGridViewAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private Context mContext;
    private ArrayList<AlbumGridViewItem> mAlbumsList;

    public AlbumsGridViewAdapter(Activity activity, ArrayList<AlbumGridViewItem> mAlbumsList) {
        mContext = activity;
        this.mAlbumsList = mAlbumsList;
    }

    public void setData(ArrayList<AlbumGridViewItem> mAlbumsList) {
        this.mAlbumsList = mAlbumsList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mAlbumsList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAlbumsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.album_thumb_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.titleAlbumField);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.thumbAlbumField);
            viewHolder.bar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            convertView.setTag(viewHolder);
        }

        holder = (ViewHolder) convertView.getTag();

        AlbumGridViewItem item = mAlbumsList.get(position);
        holder.text.setText(item.title);
        String url = HttpClient.get_SERVER_URL() + item.iconUrl;
        Picasso.with(mContext).load(url).resize(350, 350).centerCrop().into(holder.icon);


        return convertView;
    }

    static class ViewHolder {
        TextView text;
        ProgressBar bar;
        ImageView icon;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent();
        intent.setClass(mContext, UserAlbumActivity.class);
        intent.putExtra(UserAlbumFragment.ALBUM_ID, mAlbumsList.get(position).id);
        mContext.startActivity(intent);
    }
}
