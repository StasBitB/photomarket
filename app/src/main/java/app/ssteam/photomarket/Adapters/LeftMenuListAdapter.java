package app.ssteam.photomarket.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.ssteam.photomarket.R;

/**
 * Created by Stas on 27-Mar-15.
 */
public class LeftMenuListAdapter extends BaseAdapter {

    private List<String> mItems;
    private Context mContext;

    public LeftMenuListAdapter(Context context, ArrayList<String> items) {
        mItems = items;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = mInflater.inflate(R.layout.left_menu_item, null);
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.icon);

        switch (position) {
            case 0:
                imageView.setImageResource(R.drawable.albums);
                break;
            case 1:
                imageView.setImageResource(R.drawable.friends);
                break;
            case 2:
                imageView.setImageResource(R.drawable.requests);
                break;
            case 3:
                imageView.setImageResource(R.drawable.log_out);
                break;
            default:
                break;
        }


        ((TextView) v.findViewById(R.id.textField)).setText(mItems.get(position));
        return v;
    }
}
