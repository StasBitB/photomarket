package app.ssteam.photomarket.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import app.ssteam.photomarket.Adapters.AdapterEntities.ImageGridViewItem;
import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.R;

/**
 * Created by Stas on 10-Apr-15.
 */
public class ImagesGridViewAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<ImageGridViewItem> mImagesList;

    public ImagesGridViewAdapter(Context mContext, ArrayList<ImageGridViewItem> mPhotosList) {
        this.mContext = mContext;
        this.mImagesList = mPhotosList;
    }

    public void setData(ArrayList<ImageGridViewItem> list) {
        mImagesList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mImagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return mImagesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.album_thumb_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView.findViewById(R.id.titleAlbumField);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.thumbAlbumField);
            viewHolder.bar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            convertView.setTag(viewHolder);
        }

        holder = (ViewHolder) convertView.getTag();

        ImageGridViewItem item = mImagesList.get(position);
        holder.text.setText(item.name);
        String url = HttpClient.get_SERVER_URL() + item.url;
        Picasso.with(mContext).load(url).resize(350, 350).centerCrop().into(holder.icon);
        return convertView;
    }

    static class ViewHolder {
        TextView text;
        ProgressBar bar;
        ImageView icon;
    }
}
