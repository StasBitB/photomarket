package app.ssteam.photomarket.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import app.ssteam.photomarket.HTTP.HttpClient;
import app.ssteam.photomarket.HTTP.HttpRequestAPI;
import app.ssteam.photomarket.HTTP.Models.PhotoModel;
import app.ssteam.photomarket.Helper.BitmapHelper;
import app.ssteam.photomarket.Helper.UserDataHelper;
import app.ssteam.photomarket.Helper.UserEntity;
import retrofit.mime.FormUrlEncodedTypedOutput;
import retrofit.mime.TypedFile;


/**
 * Created by Sokol on 8/6/2015.
 */
public class UploadService extends Service {

    public final static String BROADCAST_NOTIFICATION = "Notification";
    public final static String ID_UPLOADED = "Photo_model_id";
    public final static String TEMP_UUID = "Temp_uuid";

    private final static String THREAD_NAME = "Thread_Name";
    private LooperHandler mLooperHandler;
    private List<PhotoModel> mUploaded;
    private final IBinder mBinder = new LocalBinder();

    private class LooperHandler extends Handler {

        public LooperHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            MessObject messObject = (MessObject) msg.obj;

            ImageUpload imageUpload = new ImageUpload(UploadService.this);
            PhotoModel photoModel = imageUpload.upload(messObject.uri);

            UploadService.this.mUploaded.add(photoModel);

            Intent intent = new Intent(BROADCAST_NOTIFICATION);
            intent.putExtra(ID_UPLOADED, photoModel.id);
            intent.putExtra(TEMP_UUID, messObject.uuid);
            sendBroadcast(intent);

        }
    }

    public class LocalBinder extends Binder {
        public UploadService getService() {
            return UploadService.this;
        }
    }

    public void addImageToQueue(Uri selectedImage, UUID uuid) {
        MessObject mesObject = new MessObject(uuid, selectedImage);
        Message msg = mLooperHandler.obtainMessage();
        msg.obj = mesObject;
        mLooperHandler.sendMessage(msg);
    }

    public PhotoModel getUploadedById(int id) {
        for (PhotoModel model : mUploaded)
            if (model.id == id)
                return model;
        return null;
    }

    public List<PhotoModel> getUploadedList() {
        return mUploaded;
    }

    public void cleanList() {
        mUploaded.clear();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread(THREAD_NAME,
                android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        thread.start();
        mLooperHandler = new LooperHandler(thread.getLooper());
        mUploaded = new LinkedList();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private class MessObject {
        public MessObject(UUID uuid, Uri uri) {
            this.uri = uri;
            this.uuid = uuid;
        }

        public UUID uuid;
        public Uri uri;
    }

    private class ImageUpload {
        private Context mContext;

        public ImageUpload(Context context) {
            this.mContext = context;
        }

        public PhotoModel upload(Uri selectedImage) {
            Bitmap preview = null;
            try {
                preview = BitmapHelper.decodeUri(mContext, selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            String path = BitmapHelper.getRealPathFromURI(UploadService.this, selectedImage);
            String fileName = path.substring(path.lastIndexOf("/") + 1).split("\\.")[0];

            File photo = new File(path);
            TypedFile image = new TypedFile("multipart/form-data", photo);

            UserEntity user = UserDataHelper.loadUserData(UploadService.this);

            FormUrlEncodedTypedOutput form = new FormUrlEncodedTypedOutput();
            form.addField("userId", String.valueOf(user.id));
            form.addField("name", String.valueOf(fileName));
            form.addField("height", String.valueOf(preview.getHeight()));
            form.addField("width", String.valueOf(preview.getWidth()));

            HttpRequestAPI api = HttpClient.getInstance();
            return api.uploadImageS(image, user.token, form);
        }
    }
}
