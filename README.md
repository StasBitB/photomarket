# README #

The main purpose of the project is creating fully-functional social network. Projected system should have next features:

•	Registration user in social network
•	Adding and editing user profile data
•	Data security
•	Possibility to view friends profile data
•	Creating and editing photo albums to share
•	Log in and log out in system

System is divided in two parts: server and client applications. Client application responsible for understandable user interface, saving logged in user data, and it enables adding user photos and editing profile.